import { Personne } from './Personne';
import { Joueur } from './Joueur';

export class Evaluation {
  public id: number;
  public personneVotanteDto: Personne;
  public evaluatedJoueurDto: Joueur;
  public dateEvaluation: Date;
  public centres: number;
  public finition: number;
  public jeuDeTete: number;
  public passesCourtes: number;
  public repriseDeVolee: number;
  public dribble: number;
  public effet: number;
  public precisionCf: number;
  public passesLongues: number;
  public controleDuBallon: number;
  public acceleration: number;
  public vitesse: number;
  public agilité: number;
  public reactivite: number;
  public equilibre: number;
  public puissanceDeFrappe: number;
  public detente: number;
  public endurance: number;
  public force: number;
  public tirDeLoin: number;
  public agressivite: number;
  public interceptions: number;
  public placement: number;
  public visionDuJeu: number;
  public penaltys: number;
  public calme: number;
  public marquage: number;
  public tacle: number;
  public tacleGlissé: number;
  public jeuMain: number;
  public jeuPied: number;
  public placementGardien: number;
  public reflexes: number;
  constructor() {
    this.centres = null;
    this.finition = null;
    this.jeuDeTete = null;
    this.passesCourtes = null;
    this.repriseDeVolee = null;
    this.dribble = null;
    this.effet = null;
    this.precisionCf = null;
    this.passesLongues = null;
    this.controleDuBallon = null;
    this.acceleration = null;
    this.vitesse = null;
    this.agilité = null;
    this.reactivite = null;
    this.equilibre = null;
    this.puissanceDeFrappe = null;
    this.detente = null;
    this.endurance = null;
    this.force = null;
    this.tirDeLoin = null;
    this.agressivite = null;
    this.interceptions = null;
    this.placement = null;
    this.visionDuJeu = null;
    this.penaltys = null;
    this.calme = null;
    this.marquage = null;
    this.tacle = null;
    this.tacleGlissé = null;
    this.jeuMain = null;
    this.jeuPied = null;
    this.placementGardien = null;
    this.reflexes = null;
  }
}
