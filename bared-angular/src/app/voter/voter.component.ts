import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { JoueurService } from '../shared/services/joueur.service';
import { Router } from '@angular/router';
import { Joueur } from '../shared/models/Joueur';

@Component({
  selector: 'app-voter',
  templateUrl: './voter.component.html',
  styleUrls: ['./voter.component.css'],
})
export class VoterComponent implements OnInit {
  public myForm: FormGroup;
  constructor(
    private joueurService: JoueurService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.myForm = this.fb.group({
      nom: '',
      prenom: '',
    });
  }

  doSend() {
    if (this.myForm.status === 'VALID') {
      console.log('Passe par voter comp');
      this.joueurService
        .getJoueurByNomOrPrenom(
          this.myForm.get('nom').value,
          this.myForm.get('prenom').value
        )
        .subscribe((joueurs: Joueur[]) => {
          if (joueurs) {
            localStorage.setItem('joueurs', JSON.stringify(joueurs));
            console.log(joueurs);
            this.router.navigate(['/voter-liste']);
          } else {
            alert('Erreur');
          }
        });
    }
  }
}
