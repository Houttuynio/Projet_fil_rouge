import { Component, OnInit } from '@angular/core';
import { Joueur } from '../shared/models/Joueur';
import { Evaluation } from '../shared/models/Evaluation';
import { JoueurService } from '../shared/services/joueur.service';
import { EvaluationService } from '../shared/services/evaluation.service';

@Component({
  selector: 'app-vue-joueur',
  templateUrl: './vue-joueur.component.html',
  styleUrls: ['./vue-joueur.component.css'],
})
export class VueJoueurComponent implements OnInit {
  evaluation: Evaluation = new Evaluation();
  evaluations: Evaluation[];
  attributs: string[];
  public joueur: Joueur;
  public moyenneGenerale: number;

  constructor(
    private joueurService: JoueurService,
    private evaluationService: EvaluationService
  ) {}

  ngOnInit(): void {
    this.joueur = JSON.parse(localStorage.getItem('evaluatedJoueur'));
    this.joueurService
      .getJoueurById(this.joueur.id)
      .subscribe((joueur: Joueur) => {
        if (joueur) {
          this.joueur = joueur;
          console.log('Avant', this.joueur);
        } else {
          alert('Erreur');
        }
      });
    this.evaluationService
      .getEvaluationsByIdJoueur(this.joueur.id)
      .subscribe((evaluations: Evaluation[]) => {
        if (evaluations) {
          this.joueur.evaluations = evaluations;
          this.joueur.moyennes = [];
          console.log('Après', this.joueur);

          Object.values(this.joueur.evaluations[0]).forEach(
            (attribut, index) => {
              if (index > 3) {
                let somme = 0;
                let nbEval = 0;
                this.joueur.evaluations.forEach((evaluation) => {
                  if (Object.values(evaluation)[index] != null) {
                    somme = somme + Object.values(evaluation)[index];
                    nbEval++;
                  }
                });
                this.joueur.moyennes[index - 4] = Math.round(
                  (20 * somme) / nbEval
                );
                if (isNaN(this.joueur.moyennes[index - 4])) {
                  this.joueur.moyennes[index - 4] = null;
                }
              }
            }
          );
          console.log('Moyennes', this.joueur.moyennes);
          let somme2 = 0;
          let nbEval2 = 0;
          this.joueur.moyennes.forEach((moyenne) => {
            if (moyenne || moyenne === 0) {
              somme2 = somme2 + moyenne;
              nbEval2++;
            }
          });
          this.moyenneGenerale = Math.round(somme2 / nbEval2);
        } else {
          alert('Erreur');
        }
      });

    this.attributs = Object.getOwnPropertyNames(this.evaluation);
  }
}
