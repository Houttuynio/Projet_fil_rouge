package fr.afpa.cda.bared.microservice;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaredMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaredMicroserviceApplication.class, args);
	}

}
