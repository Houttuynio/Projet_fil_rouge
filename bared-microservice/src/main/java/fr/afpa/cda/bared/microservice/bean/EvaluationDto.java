package fr.afpa.cda.bared.microservice.bean;

import java.util.Date;

import fr.afpa.cda.bared.microservice.dao.entities.Joueur;
import lombok.Data;

@Data
public class EvaluationDto {
	private Integer id;
	private PersonneDto personneVotanteDto;
	private Joueur evaluatedJoueurDto;
	private Date dateEvaluation;
	private Integer centres;
	private Integer finition;
	private Integer jeuDeTete;
	private Integer passesCourtes;
	private Integer repriseDeVolee;
	private Integer dribble;
	private Integer effet;
	private Integer precisionCf;
	private Integer passesLongues;
	private Integer controleDuBallon;
	private Integer acceleration;
	private Integer vitesse;
	private Integer agilité;
	private Integer reactivite;
	private Integer equilibre;
	private Integer puissanceDeFrappe;
	private Integer detente;
	private Integer endurance;
	private Integer force;
	private Integer tirDeLoin;
	private Integer agressivite;
	private Integer interceptions;
	private Integer placement;
	private Integer visionDuJeu;
	private Integer penaltys;
	private Integer calme;
	private Integer marquage;
	private Integer tacle;
	private Integer tacleGlissé;
	private Integer jeuMain;
	private Integer jeuPied;
	private Integer placementGardien;
	private Integer reflexes;
}
