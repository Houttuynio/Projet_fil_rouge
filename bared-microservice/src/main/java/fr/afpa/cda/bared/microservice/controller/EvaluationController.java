package fr.afpa.cda.bared.microservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.cda.bared.microservice.bean.EvaluationDto;
import fr.afpa.cda.bared.microservice.services.IEvaluationService;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;

@RestController
@CrossOrigin(origins = "*")
public class EvaluationController {

	@Autowired
	IEvaluationService evaluationService;

	@Autowired
	IFileSystemStorageService fileSystemStorageService;

	@GetMapping({ "/evaluations" })
	public List<EvaluationDto> getAll() {
		List<EvaluationDto> evaluations = evaluationService.findAll();
		return evaluations;
	}

	// DELETE /evaluations/{id}
	@DeleteMapping("/evaluations/{id}")
	public void supprimerEvaluation(@PathVariable Integer id) {
		evaluationService.deleteById(id);
		System.out.println("Evaluation Supprimée");
	}

	// GET /evaluations/{id} ==> getById
	@GetMapping("/evaluations/{id}")
	public EvaluationDto chercherEvaluationParId(@PathVariable Integer id, HttpServletRequest request) {
		return evaluationService.findEvaluationById(id);
	}

	@PostMapping("/evaluations")
	@ResponseStatus(value = HttpStatus.CREATED)
	public EvaluationDto save(@RequestBody EvaluationDto evaluationDto) {
		System.out.println(evaluationDto);
		return evaluationService.save(evaluationDto);
	}

	@PutMapping("/evaluations")
	@ResponseStatus(value = HttpStatus.OK)
	public EvaluationDto update(@RequestBody EvaluationDto evaluationDto) {
		System.out.println(evaluationDto);
		return evaluationService.update(evaluationDto);
	}
	
	@GetMapping({ "/evaluations/joueur/{idJoueur}" })
	public List<EvaluationDto> getByJoueurId(@PathVariable Integer idJoueur) {
		List<EvaluationDto> evaluations = evaluationService.findByJoueurId(idJoueur);
		return evaluations;
	}

}
