package fr.afpa.cda.bared.microservice.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda.bared.microservice.dao.entities.Joueur;
import fr.afpa.cda.bared.microservice.dao.entities.Personne;

@Repository
public interface IJoueurDao extends JpaRepository<Joueur, Integer> {

	// recherche par nom de methode
	Joueur findByEmailAndPassword(String login, String password);

	List<Joueur> findByNomOrPrenom(String nom, String prenom);

	List<Joueur> findJoueurByNomContainingIgnoreCaseOrPrenomContainingIgnoreCase(String Nom, String Prenom);

}
