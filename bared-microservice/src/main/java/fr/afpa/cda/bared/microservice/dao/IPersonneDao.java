package fr.afpa.cda.bared.microservice.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda.bared.microservice.dao.entities.Personne;

@Repository
public interface IPersonneDao extends JpaRepository<Personne, Integer> {

	// recherche par nom de methode
	Personne findByEmailAndPassword(String login, String password);

//	List<Personne> findByNomOrPrenom(String Nom, String Prenom);

	List<Personne> findByNomContainingIgnoreCaseOrPrenomContainingIgnoreCase(String Nom, String Prenom);
}
