package fr.afpa.cda.bared.microservice.dao.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Data
@Entity
@SequenceGenerator(name = "competence_id_seq", initialValue = 1, allocationSize = 1)
public class Competence {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "competence_id_seq")
	private Integer id;
	private String nom;
	private String domaine;
//	@OneToMany
//	private List<Evaluation> evaluations = new ArrayList<>();
}
