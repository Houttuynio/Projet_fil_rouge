package fr.afpa.cda.bared.microservice.dao.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@SequenceGenerator(name = "personne_id_seq", initialValue = 1, allocationSize = 1)
public class Personne {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personne_id_seq")
	private Integer id;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	private String clubPrefere;
	private String clubActuel;
	@Temporal(TemporalType.DATE)
	private Date dateNaissance;
	private String telephone;
	@ManyToOne
	private Adresse adresse;
}
