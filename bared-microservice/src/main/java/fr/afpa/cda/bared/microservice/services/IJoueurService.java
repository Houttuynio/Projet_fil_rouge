package fr.afpa.cda.bared.microservice.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.JoueurDto;
import fr.afpa.cda.bared.microservice.bean.PersonneDto;
import fr.afpa.cda.bared.microservice.dao.entities.Joueur;

public interface IJoueurService {
	public List<JoueurDto> findAll();

	public JoueurDto findJoueurById(Integer id);

	public void deleteById(Integer id);

	public JoueurDto findByEmailAndPassword(String login, String password);
	
	List<JoueurDto> findByNomOrPrenom(String nomOrPrenom);

	public JoueurDto update(JoueurDto joueurDto);

	public JoueurDto update(JoueurDto joueurDto, MultipartFile uneImage);

	public JoueurDto save(JoueurDto joueurDto);

	public JoueurDto save(JoueurDto joueurDto, MultipartFile uneImage);
	
	public List<JoueurDto> getJoueurByNomOrPrenom(String nom, String prenom);

}
