package fr.afpa.cda.bared.microservice.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda.bared.microservice.bean.AdresseDto;
import fr.afpa.cda.bared.microservice.dao.IAdresseDao;
import fr.afpa.cda.bared.microservice.dao.entities.Adresse;
import fr.afpa.cda.bared.microservice.dao.entities.Personne;
import fr.afpa.cda.bared.microservice.properties.StorageProperties;
import fr.afpa.cda.bared.microservice.services.IAdresseService;

@Service
public class AdresseServiceImpl implements IAdresseService {

	@Autowired
	IAdresseDao adresseDao;
	@Autowired
	private ModelMapper modelMapper;
//	@Autowired
//	IFileSystemStorageService fileSystemStorageService;
	@Autowired
	StorageProperties storageProperties;

	@Override
	public List<AdresseDto> findAll() {

		return adresseDao.findAll().stream().map(entity -> modelMapper.map(entity, AdresseDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public AdresseDto findById(Integer id) {
		return modelMapper.map(adresseDao.getOne(id), AdresseDto.class);
	}

	@Override
	public void deleteById(Integer id) {
		adresseDao.deleteById(id);

	}

	@Override
	public AdresseDto save(AdresseDto nouvelleAdresse) {

		Adresse paramToDo = modelMapper.map(nouvelleAdresse, Adresse.class);
		paramToDo = adresseDao.save(paramToDo);
		return modelMapper.map(paramToDo, AdresseDto.class);
	}

//	@Override
//	public AdresseDto save(AdresseDto adresseDto, MultipartFile uneImage) {
//		Personne utilisateurDoResult = adresseDao.save(modelMapper.map(adresseDto, Personne.class));
////		String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), utilisateurDoResult.getId());
////		Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
////		utilisateurDoResult.setPathImage(path.toString());
////		utilisateurDoResult = personneDao.save(utilisateurDoResult);
//		return modelMapper.map(utilisateurDoResult, AdresseDto.class);
//	}

	@Override
	public AdresseDto update(AdresseDto adresseModifiee) {
		Adresse paramToDo = modelMapper.map(adresseModifiee, Adresse.class);
		paramToDo = adresseDao.save(paramToDo);
		return modelMapper.map(paramToDo, AdresseDto.class);
	}

	@Override
	public AdresseDto findByAll(AdresseDto adresseDto) {
		System.out.println("0 " + adresseDto);
		Adresse paramToDo = modelMapper.map(adresseDto, Adresse.class);
		System.out.println("1 " + paramToDo);
		paramToDo = adresseDao.findAdresseByLigne1AndLigne2AndCodePostalAndVille(adresseDto.getLigne1(),
				adresseDto.getLigne2(), adresseDto.getCodePostal(), adresseDto.getVille());
		System.out.println("2 " + paramToDo);
		
		if (paramToDo!=null) {
			adresseDto=modelMapper.map(paramToDo, AdresseDto.class);
		}
		return adresseDto;
	}

//	@Override
//	public AdresseDto update(AdresseDto adresseModifiee, MultipartFile uneImage) {
//		Personne utilisateurDoResult = modelMapper.map(adresseModifiee, Personne.class);
////		String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), utilisateurDoResult.getId());
////		Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
////		utilisateurDoResult.setPathImage(path.toString());
////		utilisateurDoResult = personneDao.save(utilisateurDoResult);
//		return modelMapper.map(utilisateurDoResult, AdresseDto.class);
//	}
}
