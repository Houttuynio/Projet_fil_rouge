package fr.afpa.cda.bared.microservice.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.EvaluationDto;
import fr.afpa.cda.bared.microservice.dao.IEvaluationDao;
import fr.afpa.cda.bared.microservice.dao.entities.Evaluation;
import fr.afpa.cda.bared.microservice.properties.StorageProperties;
import fr.afpa.cda.bared.microservice.services.IEvaluationService;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;

@Service
public class EvaluationServiceImpl implements IEvaluationService {

	@Autowired
	IEvaluationDao evaluationDao;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	IFileSystemStorageService fileSystemStorageService;
	@Autowired
	StorageProperties storageProperties;

	@Override
	public List<EvaluationDto> findAll() {

		return evaluationDao.findAll().stream().map(entity -> modelMapper.map(entity, EvaluationDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public EvaluationDto findEvaluationById(Integer id) {
		Evaluation evaluation = evaluationDao.getOne(id);
		if (evaluation == null) {
			return null;
		}
		return modelMapper.map(evaluation, EvaluationDto.class);
	}

	@Override
	public void deleteById(Integer id) {
		evaluationDao.deleteById(id);

	}

	@Override
	public EvaluationDto save(EvaluationDto nouveauEvaluation) {

		Evaluation paramToDo = modelMapper.map(nouveauEvaluation, Evaluation.class);
		paramToDo = evaluationDao.save(paramToDo);
		return modelMapper.map(paramToDo, EvaluationDto.class);
	}

//	@Override
//	public EvaluationDto save(EvaluationDto evaluationDto, MultipartFile uneImage) {
//		Evaluation evaluationResult = evaluationDao.save(modelMapper.map(evaluationDto, Evaluation.class));
//		return modelMapper.map(evaluationResult, EvaluationDto.class);
//	}

	@Override
	public EvaluationDto update(EvaluationDto evaluationModifie) {
		Evaluation paramToDo = modelMapper.map(evaluationModifie, Evaluation.class);
		paramToDo = evaluationDao.save(paramToDo);
		return modelMapper.map(paramToDo, EvaluationDto.class);
	}

//	@Override
//	public EvaluationDto update(EvaluationDto personneModifiee, MultipartFile uneImage) {
//		Evaluation evaluationResult = modelMapper.map(personneModifiee, Evaluation.class);
//		return modelMapper.map(evaluationResult, EvaluationDto.class);
//	}

	@Override
	public List<EvaluationDto> findByJoueurId(Integer id) {
		return evaluationDao.findByEvaluatedJoueur_id(id).stream().map(entity -> modelMapper.map(entity, EvaluationDto.class))
				.collect(Collectors.toList());
	}

}
