package fr.afpa.cda.bared.microservice.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.microservice.bean.PersonneDto;
import fr.afpa.cda.bared.microservice.dao.IAdresseDao;
import fr.afpa.cda.bared.microservice.dao.IPersonneDao;
import fr.afpa.cda.bared.microservice.dao.entities.Adresse;
import fr.afpa.cda.bared.microservice.dao.entities.Personne;
import fr.afpa.cda.bared.microservice.properties.StorageProperties;
import fr.afpa.cda.bared.microservice.services.IFileSystemStorageService;
import fr.afpa.cda.bared.microservice.services.IPersonneService;

@Service
public class PersonneServiceImpl implements IPersonneService {

	@Autowired
	IPersonneDao personneDao;
	@Autowired
	IAdresseDao adresseDao;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	IFileSystemStorageService fileSystemStorageService;
	@Autowired
	StorageProperties storageProperties;

	@Override
	public List<PersonneDto> findAll() {

		return personneDao.findAll().stream().map(entity -> modelMapper.map(entity, PersonneDto.class))
				.collect(Collectors.toList());
	}

	@Override
	public PersonneDto findPersonneById(Integer id) {
		Personne personne = personneDao.getOne(id);
		if (personne == null) {
			return null;
		}
		return modelMapper.map(personne, PersonneDto.class);
	}

	@Override
	public void deleteById(Integer id) {
		personneDao.deleteById(id);

	}

	@Override
	public PersonneDto findByEmailAndPassword(String login, String password) {
		Personne personne = personneDao.findByEmailAndPassword(login, password);
		if (personne == null) {
			return null;
		}
		return modelMapper.map(personne, PersonneDto.class);

	}

	@Override
	public PersonneDto save(PersonneDto nouveauPersonne) {

		Personne paramToDo = modelMapper.map(nouveauPersonne, Personne.class);
		Adresse adresseDto = adresseDao.save(paramToDo.getAdresse());
		paramToDo.setAdresse(adresseDto);
		paramToDo = personneDao.save(paramToDo);
		return modelMapper.map(paramToDo, PersonneDto.class);
	}

	@Override
	public PersonneDto save(PersonneDto personneDto, MultipartFile uneImage) {
		Personne utilisateurDoResult = personneDao.save(modelMapper.map(personneDto, Personne.class));
//		String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), utilisateurDoResult.getId());
//		Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
//		utilisateurDoResult.setPathImage(path.toString());
//		utilisateurDoResult = personneDao.save(utilisateurDoResult);
		return modelMapper.map(utilisateurDoResult, PersonneDto.class);
	}

	@Override
	public PersonneDto update(PersonneDto personneModifiee) {
		// chercher l'ancien utilisateur en BDD
//		Personne oldUtilisateurDo = personneDao.findById(personneModifiee.getId()).get();
		// remettre le chemin de l'image (non updaté dans cette méthode) dans le nouvel
		// utilisateur
		Personne paramToDo = modelMapper.map(personneModifiee, Personne.class);
		
//		Adresse adresseDto = adresseDao.save(paramToDo.getAdresse());
//		paramToDo.setAdresse(adresseDto);
		
//		paramToDo.setPathImage(oldUtilisateurDo.getPathImage());
		// sauvegarder le nouvel utilisateur
		paramToDo = personneDao.save(paramToDo);
		return modelMapper.map(paramToDo, PersonneDto.class);
	}

	@Override
	public PersonneDto update(PersonneDto personneModifiee, MultipartFile uneImage) {
		Personne utilisateurDoResult = modelMapper.map(personneModifiee, Personne.class);
//		String chemin = MessageFormat.format(storageProperties.getUtilisateurImageDir(), utilisateurDoResult.getId());
//		Path path = fileSystemStorageService.storeAndGetPath(uneImage, Paths.get(chemin));
//		utilisateurDoResult.setPathImage(path.toString());
//		utilisateurDoResult = personneDao.save(utilisateurDoResult);
		return modelMapper.map(utilisateurDoResult, PersonneDto.class);
	}

//	@Override
//	public List<PersonneDto> findByNomOrPrenom(String nomOrPrenom) {
//		List<Personne> personnes = personneDao.findByNomOrPrenom(nomOrPrenom, nomOrPrenom);
//		if (personnes.size()<1 || personnes==null) {
//			return null;
//		}
//		List<PersonneDto> personnesDtos=new ArrayList<>();
//		for (Personne personne : personnes) {
//			personnesDtos.add(modelMapper.map(personne, PersonneDto.class));
//		}
//		return personnesDtos;
//	}

	@Override
	public List<PersonneDto> getPersonneByNomOrPrenom(String nom, String prenom) {
		List<Personne> personnes = personneDao.findByNomContainingIgnoreCaseOrPrenomContainingIgnoreCase(nom, prenom);
		System.out.println(personnes);
		return personneDao.findByNomContainingIgnoreCaseOrPrenomContainingIgnoreCase(nom, prenom).stream().map(entity -> modelMapper.map(entity, PersonneDto.class))
				.collect(Collectors.toList());
		
	}
}
