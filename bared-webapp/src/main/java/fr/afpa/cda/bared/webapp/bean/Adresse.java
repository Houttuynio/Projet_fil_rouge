package fr.afpa.cda.bared.webapp.bean;

import lombok.Data;

@Data
public class Adresse {
	private Integer id;
	private String ligne1;
	private String ligne2;
	private String codePostal;
	private String ville;
}
