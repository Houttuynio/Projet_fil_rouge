package fr.afpa.cda.bared.webapp.bean;

import java.util.Date;

import lombok.Data;

@Data
public class Evaluation {
	private Integer id;
	private Integer note;
	private Date date;
	private Personne personneVotante;
	private Competence competence;
	private Joueur evaluatedJoueur;
}
