package fr.afpa.cda.bared.webapp.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Joueur extends Personne {
	private Integer tailleCm;
	private Integer poidsKg;
	private Boolean piedFortDroit;
	private String pathPhoto;
	private String description;
	private Float vitesse100m;
	private List<Competence> competences = new ArrayList<>();
	private List<Evaluation> evaluations = new ArrayList<>();
	private String url;
}
