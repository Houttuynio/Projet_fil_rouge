package fr.afpa.cda.bared.webapp.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.afpa.cda.bared.webapp.bean.Adresse;
import fr.afpa.cda.bared.webapp.bean.Personne;
import fr.afpa.cda.bared.webapp.services.IPersonneService;

@Controller
public class PersonneController {
	@Autowired
	private IPersonneService personneService;
	
	@GetMapping(value = { "/", "/index" })
	public String pageDAccueil() {
		return "index_page";
	}
	
	@GetMapping("/profilPersonne")
	public String afficherProfil(HttpSession session) {
//		if (session.isNew() || ControllersUtils.getAttribut(session, "connectedUser", Utilisateur.class) == null)
		if (session.isNew() || session.getAttribute("connectedUser")==null)
			return "redirect:/profil/login";
		return "profil/profil_personne_page";
	}

	@GetMapping("/profilPersonne/modifier")
	public String afficherModifierProfil(HttpSession session) {
//		if (session.isNew() || ControllersUtils.getAttribut(session, "connectedUser", Utilisateur.class) == null)
		if (session.isNew() || session.getAttribute("connectedUser")==null)
			return "redirect:/profil/login";
		return "profil/modifier_personne_page";
	}

	@PostMapping("/profilPersonne/modifier")
	public String modifierProfilPersonne(HttpSession session/*, @RequestParam("file_image") MultipartFile file*/,
			Personne nouveauPersonne, Adresse adresse) throws IOException {
		nouveauPersonne.setAdresse(adresse);
		nouveauPersonne = personneService.update(nouveauPersonne, null);
		session.setAttribute("connectedUser", nouveauPersonne);
		return "redirect:/";
	}

	@GetMapping("/profilPersonne/inscription")
	public String afficherInscriptionProfil() {
		System.out.println("Passe");
		return "profil/nouveau_personne_page";
	}
	
	@PostMapping("/profilPersonne/inscription")
	public String inscriptionProfil(HttpSession session, Personne nouveauPersonne, Adresse adresse) throws IOException {
		nouveauPersonne.setAdresse(adresse);
		System.out.println(nouveauPersonne);
		nouveauPersonne = personneService.save(nouveauPersonne,null);
		session.setAttribute("connectedUser", nouveauPersonne);
		return "redirect:/";
	}

//	@PostMapping("/profil/inscription")
//	public String inscriptionProfil(HttpSession session, @RequestParam("file_image") MultipartFile file,
//			Personne nouveauPersonne, Adresse adresse) throws IOException {
//		nouveauPersonne.setAdresse(adresse);
//		nouveauPersonne = personneService.save(nouveauPersonne, file);
//		session.setAttribute("connectedUser", nouveauPersonne);
//		return "redirect:/profil";
//	}

	@GetMapping({"/profil/login","/login"})
	public String afficherSeLoger(/*String login, String password*/) throws Exception {
//		if(true) {
//			throw new Exception("toto");
//		}
		return "profil/login_page";
	}

	@PostMapping("/profil/login")
	public String seLoger(HttpSession session, RedirectAttributes model, String login, String password) {
		Personne connectedPersonne = personneService.findByEmailAndPassword(login, password);

		if (connectedPersonne == null) {
			model.addFlashAttribute("messageErreur", "Login/mdp invalide");
			return "redirect:/profil/login";
		}

		session.setAttribute("connectedUser", connectedPersonne);

		return "redirect:/profil";
	}

	@GetMapping("/profil/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/profil/login";
	}

	@ExceptionHandler(Exception.class)
	public String handleNotFoudPage(Exception exc) {
		exc.printStackTrace();
		return "errors/not_found";
	}
}
