package fr.afpa.cda.bared.webapp.controller.errors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import fr.afpa.cda.bared.webapp.exceptions.StorageFileNotFoundException;

@ControllerAdvice
@EnableWebMvc
public class GlobalExceptionHandler {

	@ExceptionHandler(value = { StorageFileNotFoundException.class })
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler(Exception.class)
	public String handleNotFoudPage(Exception exc) {
		exc.printStackTrace();
		return "errors/error500";
	}


	@ExceptionHandler
    @ResponseStatus(value=HttpStatus.NOT_FOUND)
    public String requestHandlingNoHandlerFound(final NoHandlerFoundException ex) {
        
		return "errors/not_found";
		
    }

}