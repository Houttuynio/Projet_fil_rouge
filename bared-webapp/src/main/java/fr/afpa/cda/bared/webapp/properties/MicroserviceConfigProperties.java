package fr.afpa.cda.bared.webapp.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@PropertySource("propertiesConfig/microservice-conf.properties")
@Configuration
public class MicroserviceConfigProperties {
	@Value("${microservice-base-url}")
	private String baseUrl;
	
	@Value("${microservice-end-point.liste-personnes}")
	private String endPointListePersonnes;
	@Value("${microservice-end-point.personne.seLoger}")
	private String endPointPersonneSeLoger;
	@Value("${microservice-end-point.personne.create}")
	private String endPointPersonneCreate;
	@Value("${microservice-end-point.personne.update}")
	private String endPointPersonneUpdate;
	@Value("${microservice-end-point.personne.delete}")
	private String endPointPersonneDelete;
	@Value("${microservice-end-point.personne.findById}")
	private String endPointPersonneFindById;
	
	@Value("${microservice-end-point.liste-joueurs}")
	private String endPointListeJoueurs;
//	@Value("${microservice-end-point.joueur.seLoger}")
//	private String endPointJoueurSeLoger;
	@Value("${microservice-end-point.joueur.create}")
	private String endPointJoueurCreate;
	@Value("${microservice-end-point.joueur.update}")
	private String endPointJoueurUpdate;
	@Value("${microservice-end-point.joueur.delete}")
	private String endPointJoueurDelete;
	@Value("${microservice-end-point.joueur.findById}")
	private String endPointJoueurFindById;
	
	@Value("${microservice-end-point.liste-evaluations}")
	private String endPointListeEvaluations;
	@Value("${microservice-end-point.evaluation.create}")
	private String endPointEvaluationCreate;
	@Value("${microservice-end-point.evaluation.update}")
	private String endPointEvaluationUpdate;
	@Value("${microservice-end-point.evaluation.delete}")
	private String endPointEvaluationDelete;
	@Value("${microservice-end-point.evaluation.findById}")
	private String endPointEvaluationFindById;
	@Value("${microservice-end-point.evaluation.findByJoueurIdAndCompetenceId}")
	private String endPointEvaluationFindByJoueurIdAndCompetenceId;
	
	@Value("${microservice-end-point.liste-competences}")
	private String endPointListeCompetences;
	@Value("${microservice-end-point.competence.create}")
	private String endPointCompetenceCreate;
	@Value("${microservice-end-point.competence.update}")
	private String endPointCompetenceUpdate;
	@Value("${microservice-end-point.competence.delete}")
	private String endPointCompetenceDelete;
	@Value("${microservice-end-point.competence.findById}")
	private String endPointCompetenceFindById;
	
	
}
