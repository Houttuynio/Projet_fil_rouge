package fr.afpa.cda.bared.webapp.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Joueur;
import fr.afpa.cda.bared.webapp.services.IJoueurService;
import fr.afpa.cda.bared.webapp.services.rest.IJoueurRest;

@Service
public class JoueurServiceImpl implements IJoueurService {
	@Autowired
	private IJoueurRest joueurRest;

	@Override
	public List<Joueur> findAll() {
		return joueurRest.findAll();
	}

	@Override
	public Joueur findById(Integer id) {
		return joueurRest.getOne(id);
	}

	@Override
	public Joueur save(Joueur nouveauJoueur,MultipartFile file) throws IOException {
		//nouveauJoueur.setCompetences(competenceService.findAll());
		return joueurRest.save(nouveauJoueur, file);
	}

	@Override
	public Joueur update(Joueur nouveauJoueur,MultipartFile file) throws IOException {
		return joueurRest.update(nouveauJoueur, file);
	}

	@Override
	public void deleteById(Integer id) {
		joueurRest.deleteById(id);		
	}

//	@Override
//	public Joueur findByEmailAndPassword(String login, String password) {
//		return joueurRest.seLoger(login, password);
//	}
	
}
