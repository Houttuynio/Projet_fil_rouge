package fr.afpa.cda.bared.webapp.services.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Personne;
import fr.afpa.cda.bared.webapp.services.IPersonneService;
import fr.afpa.cda.bared.webapp.services.rest.IPersonneRest;

@Service
public class PersonneServiceImpl implements IPersonneService {
	@Autowired
	private IPersonneRest personneRest;

	@Override
	public List<Personne> findAll() {
		return personneRest.findAll();
	}

	@Override
	public Personne findById(Integer id) {
		return personneRest.getOne(id);
	}

	@Override
	public Personne save(Personne nouvellePersonne,MultipartFile file) throws IOException {
		return personneRest.save(nouvellePersonne, file);
	}

	@Override
	public Personne update(Personne nouvellePersonne,MultipartFile file) throws IOException {
		return personneRest.update(nouvellePersonne, file);
	}

	@Override
	public void deleteById(Integer id) {
		personneRest.deleteById(id);		
	}

	@Override
	public Personne findByEmailAndPassword(String login, String password) {
		return personneRest.seLoger(login, password);
	}

//	@Override
//	public Personne update(Personne nouvellePersonne) {
//		return personneRest.update(nouvellePersonne);
//	}
//
//	@Override
//	public Personne save(Personne nouvellePersonne) {
//		return personneRest.save(nouvellePersonne);
//
//	}
	
	
}
