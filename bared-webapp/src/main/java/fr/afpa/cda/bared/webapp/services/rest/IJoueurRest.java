package fr.afpa.cda.bared.webapp.services.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Joueur;

public interface IJoueurRest {

	List<Joueur> findAll();

	Joueur getOne(Integer id);

	Joueur save(Joueur nouveauJoueur, MultipartFile file) throws IOException;

	Joueur update(Joueur nouveauJoueur, MultipartFile file) throws IOException;

	void deleteById(Integer id);

//	Joueur seLoger(String login, String password);

}
