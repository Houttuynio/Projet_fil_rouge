package fr.afpa.cda.bared.webapp.services.rest;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Personne;

public interface IPersonneRest {
	List<Personne> findAll();

	Personne getOne(Integer id);

	Personne save(Personne nouvellePersonne, MultipartFile fileImage) throws IOException;

	void deleteById(Integer id);

	Personne seLoger(String login, String password);

	Personne update(Personne nouvellePersonne, MultipartFile fileImage) throws IOException;

//	Personne save(Personne nouveauPersonne);
//
//	Personne update(Personne nouvellePersonne);
}
