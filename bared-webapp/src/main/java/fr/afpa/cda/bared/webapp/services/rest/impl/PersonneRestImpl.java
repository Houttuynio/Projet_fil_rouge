package fr.afpa.cda.bared.webapp.services.rest.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.cda.bared.webapp.bean.Personne;
import fr.afpa.cda.bared.webapp.properties.MicroserviceConfigProperties;
import fr.afpa.cda.bared.webapp.services.rest.IPersonneRest;

@Service
public class PersonneRestImpl implements IPersonneRest {

	WebTarget webTarget;
	WebTarget webTargetMultiPart;
	@Resource(name = "dateFormatApi")
	DateFormat dateFormatApi;
	MicroserviceConfigProperties microserviceConfigProperties;

	@Autowired
	public PersonneRestImpl(@Qualifier("clientJersey") Client client,
			@Qualifier("clientJerseyWithMultipart") Client clientMultipart,
			MicroserviceConfigProperties microserviceConfigProperties) {
		this.microserviceConfigProperties = microserviceConfigProperties;
		this.webTarget = client.target(microserviceConfigProperties.getBaseUrl());
		this.webTargetMultiPart = clientMultipart.target(microserviceConfigProperties.getBaseUrl());
	}

	@Override
	public List<Personne> findAll() {
		WebTarget personneWebTarget = webTarget.path(microserviceConfigProperties.getEndPointListePersonnes());
		Invocation.Builder invocationBuilder = personneWebTarget.request();
		return invocationBuilder.get(new GenericType<List<Personne>>() {
		});
	}

	@Override
	public Personne getOne(Integer id) {
		String endPointFindById = MessageFormat.format(microserviceConfigProperties.getEndPointPersonneFindById(),
				id);
		WebTarget personneWebTarget = webTarget.path(endPointFindById);
		Invocation.Builder invocationBuilder = personneWebTarget.request(MediaType.APPLICATION_JSON);
		Personne personne = invocationBuilder.get(Personne.class);
		return personne;
	}

	@Override
	public void deleteById(Integer id) {
		String endPointCrud = MessageFormat.format(microserviceConfigProperties.getEndPointPersonneDelete(), id);
		WebTarget personneWebTarget = webTarget.path(endPointCrud);
		Invocation.Builder invocationBuilder = personneWebTarget.request();
		invocationBuilder.delete();
	}

//	@Override
//	public Personne save(Personne nouveauPersonne) {
//
//		WebTarget personneWebTarget = webTarget.path(microserviceConfigProperties.getEndPointPersonneCreate());
//		Invocation.Builder invocationBuilder = personneWebTarget.request(MediaType.APPLICATION_JSON);
//		System.out.println("Personne REst " +nouveauPersonne);
//		return invocationBuilder.post(Entity.entity(nouveauPersonne, MediaType.APPLICATION_JSON), Personne.class);
//	}
//	
//	@Override
//	public Personne update(Personne updatedPersonne) {
//		WebTarget personneWebTarget = webTarget.path(microserviceConfigProperties.getEndPointPersonneUpdate());
//		Invocation.Builder invocationBuilder = personneWebTarget.request(MediaType.APPLICATION_JSON);
//		return invocationBuilder.post(Entity.entity(updatedPersonne, MediaType.APPLICATION_JSON), Personne.class);
//	}
	
	@Override
	public Personne save(Personne nouveauPersonne, MultipartFile fileImage) throws IOException {

		FormDataMultiPart formDataMultiPart = mapPersonneDtoToParamRequest(nouveauPersonne);

		if (fileImage != null && !fileImage.isEmpty()) {
			final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image",
					fileImage.getInputStream(), fileImage.getResource().getFilename());
			formDataMultiPart.bodyPart(streamDataBodyPart);
		}

		MultiPart multiPart = formDataMultiPart;
		WebTarget personneWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointPersonneCreate());// Joueur
		// Construction requete http
		Invocation.Builder invocationBuilder = personneWebTarget.request();
		return invocationBuilder.post(Entity.entity(multiPart, multiPart.getMediaType()), Personne.class);
	}

	@Override
	public Personne update(Personne nouveauPersonne, MultipartFile fileImage) throws IOException {
		FormDataMultiPart formDataMultiPart = mapPersonneDtoToParamRequest(nouveauPersonne);

		if (fileImage != null && !fileImage.isEmpty()) {
			final StreamDataBodyPart streamDataBodyPart = new StreamDataBodyPart("file_image",
					fileImage.getInputStream(), fileImage.getResource().getFilename());
			formDataMultiPart.bodyPart(streamDataBodyPart);
		}

		MultiPart multiPart = formDataMultiPart;
		WebTarget personneWebTarget = webTargetMultiPart.path(microserviceConfigProperties.getEndPointPersonneUpdate());// Joueur
		// Construction requete http
		Invocation.Builder invocationBuilder = personneWebTarget.request();
		return invocationBuilder.put(Entity.entity(multiPart, multiPart.getMediaType()), Personne.class);
	}

	@Override
	public Personne seLoger(String login, String password) {
		String endPointSeLoger = microserviceConfigProperties.getEndPointPersonneSeLoger();
		WebTarget personneWebTarget = webTarget.path(endPointSeLoger).queryParam("login", login).queryParam("password",
				password);
		Invocation.Builder invocationBuilder = personneWebTarget.request();
		return invocationBuilder.get(Personne.class);
	}

	private FormDataMultiPart mapPersonneDtoToParamRequest(Personne nouveauPersonne) {
		// utilisateur
		FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
		formDataMultiPart.field("id", nouveauPersonne.getId() != null ? nouveauPersonne.getId().toString() : "");
		formDataMultiPart.field("nom", nouveauPersonne.getNom() != null ? nouveauPersonne.getNom() : "");
		formDataMultiPart.field("prenom", nouveauPersonne.getPrenom() != null ? nouveauPersonne.getPrenom() : "");
		formDataMultiPart.field("email", nouveauPersonne.getEmail() != null ? nouveauPersonne.getEmail() : "");
		formDataMultiPart.field("password", nouveauPersonne.getPassword() != null ? nouveauPersonne.getPassword() : "");
		formDataMultiPart.field("dateNaissance",
				nouveauPersonne.getDateNaissance() != null ? dateFormatApi.format(nouveauPersonne.getDateNaissance())
						: "");
		formDataMultiPart.field("clubPrefere",
				nouveauPersonne.getClubPrefere() != null ? nouveauPersonne.getClubPrefere() : "");
		formDataMultiPart.field("clubActuel",
				nouveauPersonne.getClubActuel() != null ? nouveauPersonne.getClubActuel() : "");
		formDataMultiPart.field("telephone",
				nouveauPersonne.getTelephone() != null ? nouveauPersonne.getTelephone() : "");

		// Adresse
		if (nouveauPersonne.getAdresse() != null) {
			formDataMultiPart.field("id",
					nouveauPersonne.getAdresse().getId() != null ? nouveauPersonne.getAdresse().getId().toString() : "");
			formDataMultiPart.field("ligne1",
					nouveauPersonne.getAdresse().getLigne1() != null ? nouveauPersonne.getAdresse().getLigne1() : "");
			formDataMultiPart.field("ligne2",
					nouveauPersonne.getAdresse().getLigne2() != null ? nouveauPersonne.getAdresse().getLigne2() : "");
			formDataMultiPart.field("codePostal",
					nouveauPersonne.getAdresse().getCodePostal() != null ? nouveauPersonne.getAdresse().getCodePostal()
							: "");
			formDataMultiPart.field("ville",
					nouveauPersonne.getAdresse().getVille() != null ? nouveauPersonne.getAdresse().getVille() : "");
		}
		return formDataMultiPart;
	}

	

}
